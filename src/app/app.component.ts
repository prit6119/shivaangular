import { Component } from '@angular/core';
import { DatasrvcService } from './datasrvc.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angulartest';
  dataArr: any;
  constructor(public svc: DatasrvcService) {
    this.getData();
  }
  getData() {
    this.svc.getData().subscribe((data: any) => {
      console.log('data', data)
      this.dataArr = data;
    })
  }
}