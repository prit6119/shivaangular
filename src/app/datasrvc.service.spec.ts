import { TestBed } from '@angular/core/testing';

import { DatasrvcService } from './datasrvc.service';

describe('DatasrvcService', () => {
  let service: DatasrvcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatasrvcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
